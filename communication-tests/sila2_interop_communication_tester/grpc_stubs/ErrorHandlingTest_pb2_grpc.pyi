"""
@generated by mypy-protobuf.  Do not edit manually!
isort:skip_file
"""
from . import ErrorHandlingTest_pb2
from . import SiLAFramework_pb2
import abc
import grpc
import typing

class ErrorHandlingTestStub:
    """Tests that errors are propagated correctly"""

    def __init__(self, channel: grpc.Channel) -> None: ...
    RaiseDefinedExecutionError: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.RaiseDefinedExecutionError_Parameters,
        ErrorHandlingTest_pb2.RaiseDefinedExecutionError_Responses,
    ]
    """Raises the "Test Error" with the error message 'SiLA2_test_error_message'"""

    RaiseDefinedExecutionErrorObservably: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.RaiseDefinedExecutionErrorObservably_Parameters, SiLAFramework_pb2.CommandConfirmation
    ]
    """Raises the "Test Error" with the error message 'SiLA2_test_error_message'"""

    RaiseDefinedExecutionErrorObservably_Info: grpc.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID, SiLAFramework_pb2.ExecutionInfo
    ]
    """Monitor the state of RaiseDefinedExecutionErrorObservably"""

    RaiseDefinedExecutionErrorObservably_Result: grpc.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID, ErrorHandlingTest_pb2.RaiseDefinedExecutionErrorObservably_Responses
    ]
    """Retrieve result of RaiseDefinedExecutionErrorObservably"""

    RaiseUndefinedExecutionError: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.RaiseUndefinedExecutionError_Parameters,
        ErrorHandlingTest_pb2.RaiseUndefinedExecutionError_Responses,
    ]
    """Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'"""

    RaiseUndefinedExecutionErrorObservably: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.RaiseUndefinedExecutionErrorObservably_Parameters, SiLAFramework_pb2.CommandConfirmation
    ]
    """Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'"""

    RaiseUndefinedExecutionErrorObservably_Info: grpc.UnaryStreamMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID, SiLAFramework_pb2.ExecutionInfo
    ]
    """Monitor the state of RaiseUndefinedExecutionErrorObservably"""

    RaiseUndefinedExecutionErrorObservably_Result: grpc.UnaryUnaryMultiCallable[
        SiLAFramework_pb2.CommandExecutionUUID, ErrorHandlingTest_pb2.RaiseUndefinedExecutionErrorObservably_Responses
    ]
    """Retrieve result of RaiseUndefinedExecutionErrorObservably"""

    Get_RaiseDefinedExecutionErrorOnGet: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.Get_RaiseDefinedExecutionErrorOnGet_Parameters,
        ErrorHandlingTest_pb2.Get_RaiseDefinedExecutionErrorOnGet_Responses,
    ]
    """A property that raises a "Test Error" on get with the error message 'SiLA2_test_error_message'"""

    Subscribe_RaiseDefinedExecutionErrorOnSubscribe: grpc.UnaryStreamMultiCallable[
        ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Parameters,
        ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Responses,
    ]
    """A property that raises a "Test Error" on subscribe with the error message 'SiLA2_test_error_message'"""

    Get_RaiseUndefinedExecutionErrorOnGet: grpc.UnaryUnaryMultiCallable[
        ErrorHandlingTest_pb2.Get_RaiseUndefinedExecutionErrorOnGet_Parameters,
        ErrorHandlingTest_pb2.Get_RaiseUndefinedExecutionErrorOnGet_Responses,
    ]
    """A property that raises an Undefined Execution Error on get with the error message 'SiLA2_test_error_message'"""

    Subscribe_RaiseUndefinedExecutionErrorOnSubscribe: grpc.UnaryStreamMultiCallable[
        ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Parameters,
        ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Responses,
    ]
    """A property that raises an Undefined Execution Error on subscribe with the error message 'SiLA2_test_error_message'"""

    Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent: grpc.UnaryStreamMultiCallable[
        ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Parameters,
        ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses,
    ]
    """A property that first sends the integer value 1 and then raises a Defined Execution Error with the error message 'SiLA2_test_error_message'"""

    Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent: grpc.UnaryStreamMultiCallable[
        ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Parameters,
        ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses,
    ]
    """A property that first sends the integer value 1 and then raises a Undefined Execution Error with the error message 'SiLA2_test_error_message'"""

class ErrorHandlingTestServicer(metaclass=abc.ABCMeta):
    """Tests that errors are propagated correctly"""

    @abc.abstractmethod
    def RaiseDefinedExecutionError(
        self,
        request: ErrorHandlingTest_pb2.RaiseDefinedExecutionError_Parameters,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.RaiseDefinedExecutionError_Responses:
        """Raises the "Test Error" with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def RaiseDefinedExecutionErrorObservably(
        self,
        request: ErrorHandlingTest_pb2.RaiseDefinedExecutionErrorObservably_Parameters,
        context: grpc.ServicerContext,
    ) -> SiLAFramework_pb2.CommandConfirmation:
        """Raises the "Test Error" with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def RaiseDefinedExecutionErrorObservably_Info(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[SiLAFramework_pb2.ExecutionInfo]:
        """Monitor the state of RaiseDefinedExecutionErrorObservably"""
        pass
    @abc.abstractmethod
    def RaiseDefinedExecutionErrorObservably_Result(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.RaiseDefinedExecutionErrorObservably_Responses:
        """Retrieve result of RaiseDefinedExecutionErrorObservably"""
        pass
    @abc.abstractmethod
    def RaiseUndefinedExecutionError(
        self,
        request: ErrorHandlingTest_pb2.RaiseUndefinedExecutionError_Parameters,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.RaiseUndefinedExecutionError_Responses:
        """Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def RaiseUndefinedExecutionErrorObservably(
        self,
        request: ErrorHandlingTest_pb2.RaiseUndefinedExecutionErrorObservably_Parameters,
        context: grpc.ServicerContext,
    ) -> SiLAFramework_pb2.CommandConfirmation:
        """Raises an Undefined Execution Error with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def RaiseUndefinedExecutionErrorObservably_Info(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[SiLAFramework_pb2.ExecutionInfo]:
        """Monitor the state of RaiseUndefinedExecutionErrorObservably"""
        pass
    @abc.abstractmethod
    def RaiseUndefinedExecutionErrorObservably_Result(
        self,
        request: SiLAFramework_pb2.CommandExecutionUUID,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.RaiseUndefinedExecutionErrorObservably_Responses:
        """Retrieve result of RaiseUndefinedExecutionErrorObservably"""
        pass
    @abc.abstractmethod
    def Get_RaiseDefinedExecutionErrorOnGet(
        self,
        request: ErrorHandlingTest_pb2.Get_RaiseDefinedExecutionErrorOnGet_Parameters,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.Get_RaiseDefinedExecutionErrorOnGet_Responses:
        """A property that raises a "Test Error" on get with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def Subscribe_RaiseDefinedExecutionErrorOnSubscribe(
        self,
        request: ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Parameters,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorOnSubscribe_Responses]:
        """A property that raises a "Test Error" on subscribe with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def Get_RaiseUndefinedExecutionErrorOnGet(
        self,
        request: ErrorHandlingTest_pb2.Get_RaiseUndefinedExecutionErrorOnGet_Parameters,
        context: grpc.ServicerContext,
    ) -> ErrorHandlingTest_pb2.Get_RaiseUndefinedExecutionErrorOnGet_Responses:
        """A property that raises an Undefined Execution Error on get with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def Subscribe_RaiseUndefinedExecutionErrorOnSubscribe(
        self,
        request: ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Parameters,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorOnSubscribe_Responses]:
        """A property that raises an Undefined Execution Error on subscribe with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent(
        self,
        request: ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Parameters,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[ErrorHandlingTest_pb2.Subscribe_RaiseDefinedExecutionErrorAfterValueWasSent_Responses]:
        """A property that first sends the integer value 1 and then raises a Defined Execution Error with the error message 'SiLA2_test_error_message'"""
        pass
    @abc.abstractmethod
    def Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent(
        self,
        request: ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Parameters,
        context: grpc.ServicerContext,
    ) -> typing.Iterator[ErrorHandlingTest_pb2.Subscribe_RaiseUndefinedExecutionErrorAfterValueWasSent_Responses]:
        """A property that first sends the integer value 1 and then raises a Undefined Execution Error with the error message 'SiLA2_test_error_message'"""
        pass

def add_ErrorHandlingTestServicer_to_server(servicer: ErrorHandlingTestServicer, server: grpc.Server) -> None: ...
