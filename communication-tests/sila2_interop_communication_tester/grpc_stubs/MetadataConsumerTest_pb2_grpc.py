# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

from . import MetadataConsumerTest_pb2 as MetadataConsumerTest__pb2


class MetadataConsumerTestStub(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.EchoStringMetadata = channel.unary_unary(
            "/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/EchoStringMetadata",
            request_serializer=MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.SerializeToString,
            response_deserializer=MetadataConsumerTest__pb2.EchoStringMetadata_Responses.FromString,
        )
        self.UnpackMetadata = channel.unary_unary(
            "/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/UnpackMetadata",
            request_serializer=MetadataConsumerTest__pb2.UnpackMetadata_Parameters.SerializeToString,
            response_deserializer=MetadataConsumerTest__pb2.UnpackMetadata_Responses.FromString,
        )


class MetadataConsumerTestServicer(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature."""

    def EchoStringMetadata(self, request, context):
        """Expects the "String Metadata" metadata from the "Metadata Provider" feature and responds with its value."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")

    def UnpackMetadata(self, request, context):
        """Expects the "String Metadata" and "Two Integers Metadata" metadata from the "Metadata Provider" feature and responds with all three data items."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details("Method not implemented!")
        raise NotImplementedError("Method not implemented!")


def add_MetadataConsumerTestServicer_to_server(servicer, server):
    rpc_method_handlers = {
        "EchoStringMetadata": grpc.unary_unary_rpc_method_handler(
            servicer.EchoStringMetadata,
            request_deserializer=MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.FromString,
            response_serializer=MetadataConsumerTest__pb2.EchoStringMetadata_Responses.SerializeToString,
        ),
        "UnpackMetadata": grpc.unary_unary_rpc_method_handler(
            servicer.UnpackMetadata,
            request_deserializer=MetadataConsumerTest__pb2.UnpackMetadata_Parameters.FromString,
            response_serializer=MetadataConsumerTest__pb2.UnpackMetadata_Responses.SerializeToString,
        ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
        "sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest", rpc_method_handlers
    )
    server.add_generic_rpc_handlers((generic_handler,))


# This class is part of an EXPERIMENTAL API.
class MetadataConsumerTest(object):
    """This feature consumes SiLA Client Metadata from the "Metadata Provider" feature."""

    @staticmethod
    def EchoStringMetadata(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/EchoStringMetadata",
            MetadataConsumerTest__pb2.EchoStringMetadata_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.EchoStringMetadata_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )

    @staticmethod
    def UnpackMetadata(
        request,
        target,
        options=(),
        channel_credentials=None,
        call_credentials=None,
        insecure=False,
        compression=None,
        wait_for_ready=None,
        timeout=None,
        metadata=None,
    ):
        return grpc.experimental.unary_unary(
            request,
            target,
            "/sila2.org.silastandard.test.metadataconsumertest.v1.MetadataConsumerTest/UnpackMetadata",
            MetadataConsumerTest__pb2.UnpackMetadata_Parameters.SerializeToString,
            MetadataConsumerTest__pb2.UnpackMetadata_Responses.FromString,
            options,
            channel_credentials,
            insecure,
            call_credentials,
            compression,
            wait_for_ready,
            timeout,
            metadata,
        )
