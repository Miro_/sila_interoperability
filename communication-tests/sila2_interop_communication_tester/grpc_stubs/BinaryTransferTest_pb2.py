# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: BinaryTransferTest.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from . import SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x18\x42inaryTransferTest.proto\x12\x31sila2.org.silastandard.test.binarytransfertest.v1\x1a\x13SiLAFramework.proto\"Q\n\x1a\x45\x63hoBinaryValue_Parameters\x12\x33\n\x0b\x42inaryValue\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary\"R\n\x19\x45\x63hoBinaryValue_Responses\x12\x35\n\rReceivedValue\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary\"U\n!EchoBinariesObservably_Parameters\x12\x30\n\x08\x42inaries\x18\x01 \x03(\x0b\x32\x1e.sila2.org.silastandard.Binary\"W\n EchoBinariesObservably_Responses\x12\x33\n\x0bJointBinary\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary\"^\n,EchoBinariesObservably_IntermediateResponses\x12.\n\x06\x42inary\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary\"$\n\"Get_BinaryValueDirectly_Parameters\"`\n!Get_BinaryValueDirectly_Responses\x12;\n\x13\x42inaryValueDirectly\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary\"$\n\"Get_BinaryValueDownload_Parameters\"`\n!Get_BinaryValueDownload_Responses\x12;\n\x13\x42inaryValueDownload\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.Binary2\xd7\t\n\x12\x42inaryTransferTest\x12\xb0\x01\n\x0f\x45\x63hoBinaryValue\x12M.sila2.org.silastandard.test.binarytransfertest.v1.EchoBinaryValue_Parameters\x1aL.sila2.org.silastandard.test.binarytransfertest.v1.EchoBinaryValue_Responses\"\x00\x12\x9d\x01\n\x16\x45\x63hoBinariesObservably\x12T.sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_Parameters\x1a+.sila2.org.silastandard.CommandConfirmation\"\x00\x12v\n\x1b\x45\x63hoBinariesObservably_Info\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a%.sila2.org.silastandard.ExecutionInfo\"\x00\x30\x01\x12\xb8\x01\n#EchoBinariesObservably_Intermediate\x12,.sila2.org.silastandard.CommandExecutionUUID\x1a_.sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_IntermediateResponses\"\x00\x30\x01\x12\xa4\x01\n\x1d\x45\x63hoBinariesObservably_Result\x12,.sila2.org.silastandard.CommandExecutionUUID\x1aS.sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_Responses\"\x00\x12\xc8\x01\n\x17Get_BinaryValueDirectly\x12U.sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDirectly_Parameters\x1aT.sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDirectly_Responses\"\x00\x12\xc8\x01\n\x17Get_BinaryValueDownload\x12U.sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDownload_Parameters\x1aT.sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDownload_Responses\"\x00\x62\x06proto3')



_ECHOBINARYVALUE_PARAMETERS = DESCRIPTOR.message_types_by_name['EchoBinaryValue_Parameters']
_ECHOBINARYVALUE_RESPONSES = DESCRIPTOR.message_types_by_name['EchoBinaryValue_Responses']
_ECHOBINARIESOBSERVABLY_PARAMETERS = DESCRIPTOR.message_types_by_name['EchoBinariesObservably_Parameters']
_ECHOBINARIESOBSERVABLY_RESPONSES = DESCRIPTOR.message_types_by_name['EchoBinariesObservably_Responses']
_ECHOBINARIESOBSERVABLY_INTERMEDIATERESPONSES = DESCRIPTOR.message_types_by_name['EchoBinariesObservably_IntermediateResponses']
_GET_BINARYVALUEDIRECTLY_PARAMETERS = DESCRIPTOR.message_types_by_name['Get_BinaryValueDirectly_Parameters']
_GET_BINARYVALUEDIRECTLY_RESPONSES = DESCRIPTOR.message_types_by_name['Get_BinaryValueDirectly_Responses']
_GET_BINARYVALUEDOWNLOAD_PARAMETERS = DESCRIPTOR.message_types_by_name['Get_BinaryValueDownload_Parameters']
_GET_BINARYVALUEDOWNLOAD_RESPONSES = DESCRIPTOR.message_types_by_name['Get_BinaryValueDownload_Responses']
EchoBinaryValue_Parameters = _reflection.GeneratedProtocolMessageType('EchoBinaryValue_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _ECHOBINARYVALUE_PARAMETERS,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.EchoBinaryValue_Parameters)
  })
_sym_db.RegisterMessage(EchoBinaryValue_Parameters)

EchoBinaryValue_Responses = _reflection.GeneratedProtocolMessageType('EchoBinaryValue_Responses', (_message.Message,), {
  'DESCRIPTOR' : _ECHOBINARYVALUE_RESPONSES,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.EchoBinaryValue_Responses)
  })
_sym_db.RegisterMessage(EchoBinaryValue_Responses)

EchoBinariesObservably_Parameters = _reflection.GeneratedProtocolMessageType('EchoBinariesObservably_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _ECHOBINARIESOBSERVABLY_PARAMETERS,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_Parameters)
  })
_sym_db.RegisterMessage(EchoBinariesObservably_Parameters)

EchoBinariesObservably_Responses = _reflection.GeneratedProtocolMessageType('EchoBinariesObservably_Responses', (_message.Message,), {
  'DESCRIPTOR' : _ECHOBINARIESOBSERVABLY_RESPONSES,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_Responses)
  })
_sym_db.RegisterMessage(EchoBinariesObservably_Responses)

EchoBinariesObservably_IntermediateResponses = _reflection.GeneratedProtocolMessageType('EchoBinariesObservably_IntermediateResponses', (_message.Message,), {
  'DESCRIPTOR' : _ECHOBINARIESOBSERVABLY_INTERMEDIATERESPONSES,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.EchoBinariesObservably_IntermediateResponses)
  })
_sym_db.RegisterMessage(EchoBinariesObservably_IntermediateResponses)

Get_BinaryValueDirectly_Parameters = _reflection.GeneratedProtocolMessageType('Get_BinaryValueDirectly_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_BINARYVALUEDIRECTLY_PARAMETERS,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDirectly_Parameters)
  })
_sym_db.RegisterMessage(Get_BinaryValueDirectly_Parameters)

Get_BinaryValueDirectly_Responses = _reflection.GeneratedProtocolMessageType('Get_BinaryValueDirectly_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_BINARYVALUEDIRECTLY_RESPONSES,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDirectly_Responses)
  })
_sym_db.RegisterMessage(Get_BinaryValueDirectly_Responses)

Get_BinaryValueDownload_Parameters = _reflection.GeneratedProtocolMessageType('Get_BinaryValueDownload_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _GET_BINARYVALUEDOWNLOAD_PARAMETERS,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDownload_Parameters)
  })
_sym_db.RegisterMessage(Get_BinaryValueDownload_Parameters)

Get_BinaryValueDownload_Responses = _reflection.GeneratedProtocolMessageType('Get_BinaryValueDownload_Responses', (_message.Message,), {
  'DESCRIPTOR' : _GET_BINARYVALUEDOWNLOAD_RESPONSES,
  '__module__' : 'BinaryTransferTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.binarytransfertest.v1.Get_BinaryValueDownload_Responses)
  })
_sym_db.RegisterMessage(Get_BinaryValueDownload_Responses)

_BINARYTRANSFERTEST = DESCRIPTOR.services_by_name['BinaryTransferTest']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _ECHOBINARYVALUE_PARAMETERS._serialized_start=100
  _ECHOBINARYVALUE_PARAMETERS._serialized_end=181
  _ECHOBINARYVALUE_RESPONSES._serialized_start=183
  _ECHOBINARYVALUE_RESPONSES._serialized_end=265
  _ECHOBINARIESOBSERVABLY_PARAMETERS._serialized_start=267
  _ECHOBINARIESOBSERVABLY_PARAMETERS._serialized_end=352
  _ECHOBINARIESOBSERVABLY_RESPONSES._serialized_start=354
  _ECHOBINARIESOBSERVABLY_RESPONSES._serialized_end=441
  _ECHOBINARIESOBSERVABLY_INTERMEDIATERESPONSES._serialized_start=443
  _ECHOBINARIESOBSERVABLY_INTERMEDIATERESPONSES._serialized_end=537
  _GET_BINARYVALUEDIRECTLY_PARAMETERS._serialized_start=539
  _GET_BINARYVALUEDIRECTLY_PARAMETERS._serialized_end=575
  _GET_BINARYVALUEDIRECTLY_RESPONSES._serialized_start=577
  _GET_BINARYVALUEDIRECTLY_RESPONSES._serialized_end=673
  _GET_BINARYVALUEDOWNLOAD_PARAMETERS._serialized_start=675
  _GET_BINARYVALUEDOWNLOAD_PARAMETERS._serialized_end=711
  _GET_BINARYVALUEDOWNLOAD_RESPONSES._serialized_start=713
  _GET_BINARYVALUEDOWNLOAD_RESPONSES._serialized_end=809
  _BINARYTRANSFERTEST._serialized_start=812
  _BINARYTRANSFERTEST._serialized_end=2051
# @@protoc_insertion_point(module_scope)
