# Client Instructions
This document lists all requests that a SiLA Client has to send to the test server.
The order is irrelevant unless explicitly stated.
Additional requests are allowed.

## SiLA Service
- Execute the command **Get Feature Definition**
  - Parameter **Feature Identifier**: `"org.silastandard/core/SiLAService/v1"`
- Execute the command **Get Feature Definition**
  - Parameter **Feature Identifier**: `"org.silastandard/test/MetadataProviderTest/v1"`
- Execute the command **Set Server Name**
  - Parameter **Server Name**: `**SiLA is Awesome**`
- Read the property **Server Name**
- Read the property **Server Type**
- Read the property **Server UUID**
- Read the property **Server Description**
- Read the property **Server Version**
- Read the property **Server Vendor URL**
- Read the property **Implemented Features**

## Unobservable Command Test
- Execute the command **Command Without Parameters And Responses**
- Execute the command **Convert Integer To String**
  - Parameter **Integer**: `12345`
- Execute the command **Join Integer And String**
  - Parameter **Integer**: `123`
  - Parameter **String**: `"abc"`
- Execute the command **Split String After First Character**
  - Parameter **String**: `""`
- Execute the command **Split String After First Character**
  - Parameter **String**: `"a"`
- Execute the command **Split String After First Character**
  - Parameter **String**: `"ab"`
- Execute the command **Split String After First Character**
  - Parameter **String**: `"abcde"`

## Unobservable Property Test
- Read the property **Answer To Everything**
- Read the property **Seconds Since 1970**

## Metadata Provider
- Read the list of features, commands and properties affected by the metadata **String Metadata**
- Read the list of features, commands and properties affected by the metadata **Two Integers Metadata**

## Metadata Consumer Test
- Execute the command **Echo String Metadata**
  - Metadata **String Metadata** of feature **Metadata Provider**: `"abc"`
- Execute the command **Unpack Metadata**
  - Metadata **String Metadata** of feature **Metadata Provider**: `"abc"`
  - Metadata **Two Integers Metadata** of feature **Metadata Provider**:
    - Element **First Integer**: `123`
    - Element **Second Integer**: `456`

## Error Handling Test
- Execute the command **Raise Defined Execution Error**
- Execute the command **Raise Undefined Execution Error**
- Read the property **Raise Defined Execution Error On Get**
- Read the property **Raise Undefined Execution Error On Get**
- Initiate the observable command **Raise Defined Execution Error Observably** and request its result
- Initiate the observable command **Raise Undefined Execution Error Observably** and request its result
- Subscribe to the property **Raise Defined Execution Error On Subscription** and request its first value
- Subscribe to the property **Raise Undefined Execution Error On Subscription** and request its first value
- Subscribe to the property **Raise Defined Execution Error After Value Was Sent** and request its first two values
- Subscribe to the property **Raise Undefined Execution Error After Value Was Sent** and request its first two values

## Observable Property Test
- Get the value of the property **Fixed Value**
- Subscribe to the property **Alternating** and request its first three values
- Subscribe to the property **Editable** and then use the command **Set Value** to set the property values consecutively to `1`, `2`, and `3` while the subscription is active

## Observable Command Test
- Execute the command **Count**
  - Parameter **N**: `5`
  - Parameter **Delay**: `1`
  - Immediately after command initiation:
    - Subscribe to its execution information and request all information updates until the command finishes
    - Subscribe to its intermediate responses and request all values until the command finishes
  - After the command finishes:
    - Request the command responses
- Execute the command **Echo Value After Delay**
  - Parameter **Value**: `3`
  - Parameter **Delay**: `5`
  - Immediately after command initiation:
    - Subscribe to its execution information and request all information updates until the command finishes
  - After the command finishes:
    - Request the command responses

## Binary Transfer Test
- Get the value of the property **Binary Value Directly**
- Get the value of the property **Binary Value Download**
- Execute the command **Echo Binary Value**
  - Parameter **Binary**: String `"abc"`, UTF-8-encoded
- Execute the command **Echo Binary Value**
  - Parameter **Binary**: String `"abc"` repeated 1,000,000 times, UTF-8-encoded
- Execute the command **Echo Binaries Observably**
  - Parameter **Binaries**:
    - String `"abc"`, UTF-8-encoded
    - String `"abc"` repeated 1,000,000 times, UTF-8-encoded
    - String `"SiLA2_Test_String_Value"`, UTF-8-encoded
  - Immediately after command initiation:
    - Subscribe to its intermediate responses and request all values until the command finishes
  - After the command finishes:
    - Request the command responses
