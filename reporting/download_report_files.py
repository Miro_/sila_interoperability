import os
import json
import urllib.request
import urllib.error

report_sources = json.load(open("report-sources.json"))

os.makedirs("report-webpage/report-xmls", exist_ok=True)
os.makedirs("report-webpage/report-xmls/client-tests", exist_ok=True)
os.makedirs("report-webpage/report-xmls/server-tests", exist_ok=True)

for name, url in report_sources["test-client-reports"].items():
    try:
        urllib.request.urlretrieve(url, "report-webpage/report-xmls/server-tests/" + name + ".xml")
    except urllib.error.HTTPError:
        pass

for name, url in report_sources["test-server-reports"].items():
    try:
        urllib.request.urlretrieve(url, "report-webpage/report-xmls/client-tests/" + name + ".xml")
    except urllib.error.HTTPError:
        pass
