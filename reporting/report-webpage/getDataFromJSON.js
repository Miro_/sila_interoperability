///** JavaScript to get the data of the JSON-file: */

//** This function gets the data from the report-sources.json document: */
async function getJSON() {
    const response = await fetch('../report-sources.json');
    const data = await response.json();
    return data;
}


//** Main-Function: Gets rendered when the user presses the "Get JSON" button: */
async function createJSONofData() {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            getTestSuiteFromXML(this);
            createJSONObject(this);
            createTable(this);
        }
    };

    //** Read and open all the XML-Files from the report-sources.json: */
    const data = await getJSON();
    let xmlFiles = [];

    //** Get the client-test report-files (from report-sources.json) and adds them to the xmlFiles-Array: */
    //todo What happens if a key/name hasn't got a document?
    const testClientReports = data[Object.keys(data)[0]];
    const testClientReportsKeys = Object.keys(testClientReports);
    for (i = 0; i < testClientReportsKeys.length; i++) {
        xmlhttp.open("GET", "report-xmls/client-tests/"+ testClientReportsKeys[i] + ".xml", true);
        xmlFiles.push("report-xmls/client-tests/"+ testClientReportsKeys[i] + ".xml");
    }

    //** Get the server-test report-files (from report-sources.json) and adds them to the xmlFiles-Array: */
    const testServerReports = data[Object.keys(data)[1]];
    const testServerReportsKeys = Object.keys(testServerReports);
    for (i = 0; i < testServerReportsKeys.length; i++) {
        xmlhttp.open("GET", "report-xmls/server-tests/"+ testServerReportsKeys[i] +".xml", true);
        xmlFiles.push("report-xmls/server-tests/"+ testServerReportsKeys[i] +".xml");
    }

    // xmlhttp.open("GET", "report-xmls/client-tests/Example 1.xml", true);
    xmlhttp.send();
    console.log(xmlFiles);
    console.log("Data got imported");
}



//** Function to load and show the Test-Suite-Name: */
function getTestSuiteFromXML(xml) {
    const xmlDoc = xml.responseXML;
    const testSuiteTag = xmlDoc.getElementsByTagName('testsuite');
    let txt = "";
    txt += testSuiteTag[0].getAttribute('name') + "<br>";
    document.getElementById("test-suite").innerHTML = txt;
}



//** Function to create the JSON-Object with the data of the XML-File: */
function createJSONObject(xml) {
    const xmlDoc = xml.responseXML;
    const testSuiteTag = xmlDoc.getElementsByTagName('testsuite');
    const testcases = xmlDoc.getElementsByTagName('testcase');
    
    //** Create the JSON-Object: */
    const result = {};

    //** Put the name of the testsuite into the JSON-Object: */
    result.title = testSuiteTag[0].getAttribute('name');

    //** Create the array of the tests: */
    result.tests = [];

    //** Put the testcases into the JSON-Object: */
    for (i = 0; i < testcases.length; i++) {
        const name = testcases[i].getAttribute('name');
        const classname = testcases[i].getAttribute('classname');
        const failure = testcases[i].getElementsByTagName('failure');
        let passed = false;
        if (failure.length >= 1) {
            passed = false;
        } else {
            passed = true;
        }
        const testcase = {name, classname, passed};
        result.tests.push(testcase);
    }
    

    // console.log(result);
    return result;
}


//todo Create the table with the JSON-objects:
function createTable(xml) {
    const xmlDoc = xml.responseXML;
    const result = createJSONObject(xml);
    console.log(result);
}



// xml = read_xml(url)
// result = {}
// result.title = ...
// result.tests = []
// for testcase_element in xml.testsuites.testsuite:
//     testcase_result = {}
//     ...
//     result.tests.push(testcase_result)
// return result

// int[]  a = new int[10];
// a = new int[12];

// class ArrayList implements List {
//     public void add(...) {
//         ...
//     }
// }

// interface List {
//     public void add(...);
// }
