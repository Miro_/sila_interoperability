///** JavaScript for the Reporting-Webpage */

//todo getting the table done in the correct way
//todo open all files at once
//todo Put the "Number of testcases" and "number of fails" at the end of the table
//todo Hover-Effect on the error-messages
//todo summary of the same testcases
//todo applying a design

//** General function to load the data of the xml-file into the webpage: */
function loadXMLDoc() {
    const xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            getTestSuiteFromXML(this);
            getNumberofTestcases(this)
            getTestcasesFromXML(this);
            classifyTestcaseData(this);
            getNumberofFails();
            // getErrorMessageWithTooltip(this);
        }
    };
    xmlhttp.open("GET", "client-test-report.xml", true);
    // xmlhttp.open("GET", "https://gitlab.com/SiLA2/sila_python/-/jobs/artifacts/master/raw/server-test-report.xml?job=compatibility-server-test", true);
    // xmlhttp.open("GET", "server-test-report.xml", true);
    xmlhttp.send();
    console.log("Data got imported");
}
  
  

  //** Function to load and show the Test-Suite-Name: */
  function getTestSuiteFromXML(xml) {
    const xmlDoc = xml.responseXML;
    const testSuiteTag = xmlDoc.getElementsByTagName('testsuite');
    let txt = "";
    txt += testSuiteTag[0].getAttribute('name') + "<br>";
    document.getElementById("test-suite").innerHTML = txt;
  }



  //** Function to sort the current XML-file, whether it is a server-test-report or a client-test-report: */
  function sortXMLFile() {
    const xmlDoc = xml.responseXML;
    const testsuiteName = xmlDoc.getElementsByTagName('testsuite');
    if (testsuiteName.textContent.includes('')) {

    }
  }



  //** Function to get the number of all testcases for the current testsuite: */
  function getNumberofTestcases(xml) {
    const xmlDoc = xml.responseXML;
    const testcases = xmlDoc.getElementsByTagName('testcase');
    let numberTestcases = testcases.length;
    let txt = numberTestcases;
    document.getElementById("number-of-testcases").innerHTML = txt;
  }


  //** Function to load and show the testcases (classname and name) into the existing table: */
  function getTestcasesFromXML(xml) {
    const xmlDoc = xml.responseXML;
    let table = "<tr><th>Testcase</th><th>Camunda</th><th>Tecan</th><th>C++</th><th>C#</th><th>Java</th><th>JavaScript</th><th>Python</th></tr>";
    const testcase = xmlDoc.getElementsByTagName('testcase');
    for (i = 0; i < testcase.length; i++) { 
        // table = table + "<tr><td>" +
        table = table + '<tr id="testcase-row-' + i + '"><td>' +
        testcase[i].getAttribute('classname') + "<br>" +
        testcase[i].getAttribute('name') +
        "</td></tr>";
    }
    document.getElementById("reporting-table").innerHTML = table;
  }



  //** Function to classify the data of the testcases: */
  function classifyTestcaseData(xml) {
    const xmlDoc = xml.responseXML;
    const testcase = xmlDoc.getElementsByTagName('testcase');
    for (i = 0; i < testcase.length; i++) {
        if (testcase[i].textContent == "") {
          //! console.log("Test is fine.");
          const row = document.getElementById("testcase-row-"+i);
          let output = row.insertCell(-1);
          output.innerHTML = "Test is fine.";
          // document.getElementById("testcase-row-"+ i).innerHTML = 'Test is fine.';
        } else {
          //! console.log("Critical");
          //! console.log(testcase[i].textContent);
          const row = document.getElementById("testcase-row-"+i);
          let output = row.insertCell(-1);
          output.innerHTML = "Critical";
        }
    }

    //** Give the cells with the data from the XML-file an ID:
    const cellId = document.getElementsByTagName('td');
    for (i = 0; i < cellId.length; i++) {
      if(cellId[i].textContent.includes('Test is fine.') || cellId[i].textContent.includes('Critical')){
        cellId[i].setAttribute('id', 'testcase-cell-' + (i - 1)/2);
      } else {
        cellId[i].setAttribute('id', 'testcase-name-' + i/2);
      }
    }

    //** Give the cells with the error-message a seperate class: */
    const errorCellId = document.getElementsByTagName('td');
    for (i = 0; i < errorCellId.length; i++) {
      if (errorCellId[i].textContent.includes('Critical')) {
        errorCellId[i].setAttribute('class', 'error-case-cell');
      }
    }
  }



  //** Function to get the number of failed testcases for the current testsuite: */
  function getNumberofFails() {
    const cell = document.getElementsByTagName("td");
    let allFails = 0;
    let txt = "";

    for (i = 0; i < cell.length; i++) {
      allFails += cell[i].textContent.includes('Critical');
    }

    console.log(allFails);
    txt += allFails;
    document.getElementById("number-of-fails").innerHTML = txt;
  }



  //** Function for the Tooltip-Box which appears while the user is hovering over the failed tests: */
  //! The function won't be called at this moment (because it hasn't finished yet)!
  //todo At this moment, the index of the cell with the issue got a different index than the index of the td-elements
  function getErrorMessageWithTooltip(xml) {
    const xmlDoc = xml.responseXML;
    let cellId = document.getElementsByTagName('td');
    let errorCellId = document.getElementsByClassName('error-case-cell');
    
    // Give the td-elements with the error-message a <span>-tag:
    for (i = 0; i < cellId.length; i++) {
      if (cellId[i].textContent.includes('Critical')) {
        let nodeErrorMessage = document.createElement("span");
        let textNodeErrorMessage = document.createTextNode("123");
        nodeErrorMessage.appendChild(textNodeErrorMessage);
        cellId[i].appendChild(nodeErrorMessage);
        
        // cellId[i].setAttribute('id', 'tooltip-box');
      }
    }

    // Applying the styles for the tooltip-box:
  }